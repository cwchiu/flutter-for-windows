import 'dart:ffi';

import 'package:ffi/ffi.dart';
import 'package:win32/win32.dart';

List<String> getNetworkInterfaces() {
  List<String> interfaces = [];
  // Initialize COM
  var hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);
  if (FAILED(hr)) {
    throw WindowsException(hr);
  }

  final netManager = NetworkListManager.createInstance();
  final nlmConnectivity = calloc<Int32>();
  final enumPtr = calloc<COMObject>();
  final netPtr = calloc<COMObject>();
  final descPtr = calloc<Pointer<Utf16>>();
  final elements = calloc<Uint32>();

  try {
    hr = netManager.GetConnectivity(nlmConnectivity);
    if (FAILED(hr)) {
      throw WindowsException(hr);
    }

    final connectivity = nlmConnectivity.value;
    var isInternetConnected = false;

    // These two options are not mutually exclusive
    if (connectivity & NLM_CONNECTIVITY.NLM_CONNECTIVITY_IPV4_INTERNET ==
        NLM_CONNECTIVITY.NLM_CONNECTIVITY_IPV4_INTERNET) {
      // print('Connected to the Internet via IPv4.');
      isInternetConnected = true;
    }

    if (connectivity & NLM_CONNECTIVITY.NLM_CONNECTIVITY_IPV6_INTERNET ==
        NLM_CONNECTIVITY.NLM_CONNECTIVITY_IPV6_INTERNET) {
      // print('Connected to the Internet via IPv6.');
      isInternetConnected = true;
    }

    if (!isInternetConnected) {
      // print('Not connected to the Internet.');
    }

    hr = netManager.GetNetworks(
        NLM_ENUM_NETWORK.NLM_ENUM_NETWORK_ALL, enumPtr.cast());
    if (FAILED(hr)) {
      throw WindowsException(hr);
    }

    // print('\nNetworks (connected and disconnected) on this machine:');
    final enumerator = IEnumNetworks(enumPtr);
    hr = enumerator.Next(1, netPtr.cast(), elements);
    while (elements.value == 1) {
      final network = INetwork(netPtr);
      hr = network.GetDescription(descPtr);
      if (SUCCEEDED(hr)) {
        // print(descPtr.value.toDartString());
        interfaces.add(descPtr.value.toDartString());
      }
      hr = enumerator.Next(1, netPtr.cast(), elements);
    }
  } finally {
    free(elements);
    free(netPtr);
    free(enumPtr);
    free(descPtr);
    free(nlmConnectivity);
    free(netManager.ptr);

    CoUninitialize();
  }

  return interfaces;
}


String generateGUID(){
  final guid = calloc<GUID>();

  final hr = CoCreateGuid(guid);
  if (SUCCEEDED(hr)) {
    return guid.ref.toString();
  }

  free(guid);
  return "Error!!";
}
