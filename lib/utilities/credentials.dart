import 'dart:convert';
import 'dart:ffi';
import 'dart:typed_data';

import 'package:ffi/ffi.dart';
import 'package:win32/win32.dart';

class CredentialException implements Exception {
  final String message;
  CredentialException(this.message);

  @override
  String toString() {
    return message;
  }
}

class CerdData {
  final String username;
  final String password;
  CerdData(this.username, this.password);
}

void credentialWrite({required String credentialName, required CerdData data}) {
  final examplePassword = utf8.encode(data.password) as Uint8List;
  final blob = examplePassword.allocatePointer();

  final credential = calloc<CREDENTIAL>()
    ..ref.Type = CRED_TYPE_GENERIC
    ..ref.TargetName = credentialName.toNativeUtf16()
    ..ref.Persist = CRED_PERSIST_LOCAL_MACHINE
    ..ref.UserName = data.username.toNativeUtf16()
    ..ref.CredentialBlob = blob
    ..ref.CredentialBlobSize = examplePassword.length;

  final result = CredWrite(credential, 0);

  if (result != TRUE) {
    final errorCode = GetLastError();
    throw CredentialException('Error ($result): $errorCode');
  }

  free(blob);
  free(credential);
}

CerdData credentialRead(String credentialName) {
  final credPointer = calloc<Pointer<CREDENTIAL>>();
  final result = CredRead(
      credentialName.toNativeUtf16(), CRED_TYPE_GENERIC, 0, credPointer);
  if (result != TRUE) {
    final errorCode = GetLastError();
    var errorText = '$errorCode';
    if (errorCode == ERROR_NOT_FOUND) {
      errorText += ' Not found.';
    }
    throw CredentialException('Error ($result): $errorText');
  }
  final cred = credPointer.value.ref;
  final blob = cred.CredentialBlob.asTypedList(cred.CredentialBlobSize);
  final password = utf8.decode(blob);

  CredFree(credPointer.value);
  free(credPointer);

  return CerdData(cred.UserName.toDartString(), password);
}

void credentialDelete(String credentialName) {
  final result =
      CredDelete(credentialName.toNativeUtf16(), CRED_TYPE_GENERIC, 0);
  if (result != TRUE) {
    final errorCode = GetLastError();
    throw CredentialException('Error ($result): $errorCode');
  }
}
