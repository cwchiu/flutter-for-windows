import 'dart:ffi';

import 'package:ffi/ffi.dart';
import 'package:win32/win32.dart';

List<String> fontNames = [];

int enumerateFonts(
    Pointer<LOGFONT> logFont, Pointer<TEXTMETRIC> _, int __, int ___) {
  // Get extended information from the font
  final logFontEx = logFont.cast<ENUMLOGFONTEX>();

  fontNames.add(logFontEx.ref.elfFullName);
  return TRUE; // continue enumeration
}

List<String> getInsatlledFonts() {
  final hDC = GetDC(NULL);
  final searchFont = calloc<LOGFONT>()..ref.lfCharSet = ANSI_CHARSET;
  final callback = Pointer.fromFunction<EnumFontFamExProc>(enumerateFonts, 0);

  fontNames.clear();
  EnumFontFamiliesEx(hDC, searchFont, callback, 0, 0);

  free(searchFont);

  return fontNames;
}
