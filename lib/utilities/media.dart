import 'dart:io';

import 'package:win32/win32.dart';

class MediaException implements Exception {
  String _msg = "";
  MediaException(String message) {
    _msg = message;
  }

  @override
  String toString() {
    return _msg;
  }
}

void windowsPlaySound(String wav) {
  final file = File(wav).existsSync();

  if (!file) {
    throw MediaException("$wav is Invalid");
  } else {
    final pszLogonSound = TEXT(wav);
    final result = PlaySound(pszLogonSound, NULL, SND_FILENAME | SND_SYNC);

    if (result != TRUE) {
      throw MediaException("Sound playback failed.");
    }
    free(pszLogonSound);
  }
}
