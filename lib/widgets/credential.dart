import 'package:flutter/material.dart';
import "../utilities/credentials.dart";
import "../utilities/dialog.dart";
import "../utilities/com.dart";

class CredentialWidget extends StatefulWidget {
  const CredentialWidget({Key? key}) : super(key: key);

  @override
  State<CredentialWidget> createState() {
    return _CredentialWidgetState();
  }
}

class _CredentialWidgetState extends State<CredentialWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextButton(
          style: TextButton.styleFrom(
            textStyle: const TextStyle(fontSize: 20),
          ),
          child: const Text('讀取'),
          onPressed: () async {
            try {
              final data = credentialRead("flutter-test");
              showMyDialog(context, ">${data.username}/${data.password}<");
            } catch (e) {
              showMyDialog(context, "Error: $e");
            }
          },
        ),
        TextButton(
          style: TextButton.styleFrom(
            textStyle: const TextStyle(fontSize: 20),
          ),
          child: const Text('寫入'),
          onPressed: () async {
            try {
              credentialWrite(
                  credentialName: "flutter-test",
                  data: CerdData("flutter", generateGUID()));
              showMyDialog(context, "Ok");
            } catch (e) {
              showMyDialog(context, "Error: $e");
            }
          },
        ),
        TextButton(
          style: TextButton.styleFrom(
            textStyle: const TextStyle(fontSize: 20),
          ),
          child: const Text('刪除'),
          onPressed: () async {
            credentialDelete("flutter-test");
            showMyDialog(context, "Ok");
          },
        ),
      ],
    );
  }
}
