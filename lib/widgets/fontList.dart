// ignore: file_names
import 'package:flutter/material.dart';
import "../utilities/font.dart";

class FontListWidget extends StatefulWidget {
  const FontListWidget({Key? key}) : super(key: key);

  @override
  State<FontListWidget> createState() {
    return _FontListWidgetState();
  }
}

class _FontListWidgetState extends State<FontListWidget> {
  List<String> fonts = [];

  Widget _buildFonts(String name) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        color: Colors.white10,
        child: ListTile(
          contentPadding: const EdgeInsets.all(5),
          dense: true,
          title: Text(
            name,
          ),
        ),
      ),
    );
  }

  @override
  initState() {
    super.initState();
    setState(() {
      fonts = getInsatlledFonts();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Text(
        '已安裝字型',
        style: TextStyle(fontSize: 20, color: Color(0xff00ee00)),
      ),
      SizedBox(
        width: 400.0,
        height: 300.0,
        child: ListView(
          children: fonts.map(_buildFonts).toList(),
        ),
      ),
    ]);
  }
}
