// ignore_for_file: file_names

import 'package:flutter/material.dart';
import "../utilities/registry.dart";

class PrinterListWidget extends StatefulWidget {
  const PrinterListWidget({Key? key}) : super(key: key);

  @override
  State<PrinterListWidget> createState() {
    return _PrinterWidgetState();
  }
}

class _PrinterWidgetState extends State<PrinterListWidget> {
  List<String> printers = [];

  Widget _buildPrinter(String name) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        color: Colors.white10,
        child: ListTile(
          contentPadding: const EdgeInsets.all(5),
          dense: true,
          title: Text(
            name,
          ),
        ),
      ),
    );
  }

  @override
  initState() {
    super.initState();
    setState(() {
      final osDevices = getDevices();
      for (final device in osDevices.keys) {
        printers.add(osDevices[device]!);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Text('印表機裝置', style: TextStyle(fontSize: 20)),
      SizedBox(
        width: 400.0,
        height: 300.0,
        child: ListView(
          children: printers.map(_buildPrinter).toList(),
        ),
      ),
    ]);
  }
}
