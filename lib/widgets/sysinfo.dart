import 'package:flutter/material.dart';
import 'package:win32/win32.dart';

import "../utilities/wininfo.dart";
import "../utilities/com.dart";

class SysInfoWidget extends StatefulWidget {
  const SysInfoWidget({Key? key}) : super(key: key);

  @override
  State<SysInfoWidget> createState() {
    return _SysInfoWidget();
  }
}

class _SysInfoWidget extends State<SysInfoWidget> {
  String osVersion = "Unknown";
  int osMemoryPhysically = 0;
  int cpuNum = 0;
  String osComputerName = "Unknown";
  String guid = "";

  @override
  initState() {
    super.initState();
    setState(() {
      osVersion = getWindowsVersion();
      osMemoryPhysically = getSystemMemoryInMegabytes();
      cpuNum = GetActiveProcessorCount(ALL_PROCESSOR_GROUPS);
      osComputerName = getComputerName();
      guid = generateGUID();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text('Windows 版本: $osVersion'),
      Text('實體記憶體: ${osMemoryPhysically}MB'),
      Text('CPU 數量: $cpuNum'),
      Text('電腦名稱: $osComputerName'),
      Text('GUID: $guid'),
    ]);
  }
}
