// ignore_for_file: file_names

import 'package:flutter/material.dart';
import "../utilities/com.dart";

class NetworkListWidget extends StatefulWidget {
  const NetworkListWidget({Key? key}) : super(key: key);

  @override
  State<NetworkListWidget> createState() {
    return _NetworkWidgetState();
  }
}

class _NetworkWidgetState extends State<NetworkListWidget> {
  List<String> networks = [];

  @override
  initState() {
    super.initState();
    setState(() {
      networks = getNetworkInterfaces();
    });
  }

  Widget _buildNetworkName(String name) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        color: Colors.white10,
        child: ListTile(
          contentPadding: const EdgeInsets.all(5),
          dense: true,
          title: Text(
            name,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Text(
        '已連線網路',
        style: TextStyle(fontSize: 20, color: Color(0xffee0000)),
      ),
      SizedBox(
        width: 400.0,
        height: 300.0,
        child: ListView(
          children: networks.map(_buildNetworkName).toList(),
        ),
      ),
    ]);
  }
}
