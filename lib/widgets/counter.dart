import 'package:flutter/material.dart';

class CounterWidget extends StatefulWidget {
  const CounterWidget({Key? key}) : super(key: key);

  @override
  State<CounterWidget> createState() {
    return _CounterWidgetState();
  }
}

class _CounterWidgetState extends State<CounterWidget> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      TextButton(
        style: TextButton.styleFrom(
          textStyle: const TextStyle(fontSize: 20),
        ),
        child: const Text('+'),
        onPressed: () async {
          setState(() {
            _counter++;
          });
        },
      ),
      Text(
        '$_counter',
        style: Theme.of(context).textTheme.headline4,
      ),
      TextButton(
        style: TextButton.styleFrom(
          textStyle: const TextStyle(fontSize: 20),
        ),
        child: const Text('-'),
        onPressed: () async {
          setState(() {
            _counter--;
          });
        },
      ),
    ]);
  }
}
