import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:win32/win32.dart';

import "utilities/media.dart";
import "utilities/dialog.dart";
import "utilities/com.dart";

import "widgets/sysinfo.dart";
import "widgets/printerList.dart";
import "widgets/networkList.dart";
import "widgets/fontList.dart";
import "widgets/counter.dart";
import "widgets/credential.dart";

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo by Arick'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const CounterWidget(),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                          style: TextButton.styleFrom(
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          child: const Text('Windows API: Guid()'),
                          onPressed: () async {
                            setState(() {
                              showMyDialog(context, generateGUID());
                            });
                          },
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          child: const Text('Windows API: 播放 wav'),
                          onPressed: () async {
                            try {
                              windowsPlaySound(
                                  r"C:\Windows\Media\Windows Logon.wav");
                            } catch (e) {
                              showMyDialog(context, "$e");
                            }
                          },
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          child: const Text('Windows API: MessageBox()'),
                          onPressed: () async {
                            final hwnd = GetForegroundWindow();
                            final pMessage = 'Hello, 你好'.toNativeUtf16();
                            final pTitle =
                                'Application Documents'.toNativeUtf16();

                            MessageBox(hwnd, pMessage, pTitle, MB_OK);

                            free(pMessage);
                            free(pTitle);
                          },
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          child: const Text('Flutter AlertDialog'),
                          onPressed: () async {
                            showMyDialog(context, "Hello, 你好");
                          },
                        ),
                      ]),
                  const SysInfoWidget()
                ],
              ),
              const CredentialWidget(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  PrinterListWidget(),
                  NetworkListWidget(),
                  FontListWidget(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
